---
home: true
sidebar: auto
heroImage: /dev-ops.jpg
heroText: DevOps
tagline: Docker Container Dokumentation ;-)
features:
- title: Minimal nodeJS
  details: Minimal image with nodeJS project.
- title: Apostophe image based on Minimal nodeJS
  details: Create apostrophe image with nodejs image.
- title: Performant
  details: Yeah!. Rockt.
actionText: Get Started →
actionLink: /guide/

footer: MIT Licensed | Copyright © 2019-present R. Kuepper
---
Sa  2 Feb 2019 00:45:29 CET

## Dokumentations Übersicht

   * [Docker Container und Images benutzen](/guide/)
   * [Redhat RPM Pakete installieren](/redhat/)


::: tip
This is a tip
:::

::: warning
This is a warning
:::

::: danger
This is a dangerous warning
:::
