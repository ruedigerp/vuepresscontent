---
home: true
sidebar: auto
heroImage: /dev-ops.jpg
heroText: Redhat Systeme
tagline: Tools und Wissen
#features:
#- title: Minimal nodeJS
#  details: Minimal image with nodeJS project.
#- title: Apostophe image based on Minimal nodeJS
#  details: Create apostrophe image with nodejs image.
#- title: Performant
#  details: Yeah!. Rockt.
# actionText: Get Started →
# actionLink: /guide/

footer: MIT Licensed | Copyright © 2019-present R. Kuepper
---


## Dokumentations Übersicht.

   * [ RPM Pakete installieren](/redhat/rpm-install.html)
   * [ Firewall](/redhat/firewall.html)
