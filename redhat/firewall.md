---
home: true
sidebar: auto
heroImage: /dev-ops.jpg
heroText: Redhat
tagline: Firewall
# actionText: Get Started →
# actionLink: /guide/

footer: MIT Licensed | Copyright © 2019-present R. Kuepper
---


## Redhat Firewall

### HTTP/HTTPS freischalten

Standard ist http/https in der Firewall diabled.
Um den Server von extern freizuschalten wird das Tool firewall-cmd benutzt.

``` bash
$> firewall-cmd --permanent --zone=public --add-service
$> firewall-cmd --permanent --zone=public --add-service=https
```
