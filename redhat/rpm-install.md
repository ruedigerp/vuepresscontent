---
home: true
sidebar: auto
heroImage: /dev-ops.jpg
heroText: DevOps
tagline: Redhat RPM Pakete installieren.
# actionText: Get Started →
# actionLink: /guide/

footer: MIT Licensed | Copyright © 2019-present R. Kuepper
---


## Redhat 7.9 php Pakete installieren

### Paket installieren

``` bash
$> yum install rh-php72-php-mbstring.x86_64
```

### Testen ob das Modul in PHP angekommen ist.

``` bash
$> /usr/bin/php --modules
[PHP Modules]
bz2
calendar
Core
ctype
curl
date
dom
exif
fileinfo
filter
ftp
gd
gettext
hash
iconv
intl
json
libxml
mbstring
mysqli
mysqlnd
openssl
pcntl
pcre
PDO
pdo_mysql
pdo_sqlite
Phar
posix
readline
Reflection
session
shmop
SimpleXML
sockets
SPL
sqlite3
standard
sysvmsg
sysvsem
sysvshm
tokenizer
wddx
xml
xmlreader
xmlwriter
xsl
zip
zlib

[Zend Modules]
```

### Apache neustartet

``` bash
$> systemctl restart httpd.service
```
