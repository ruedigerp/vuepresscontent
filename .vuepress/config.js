// module.exports = {
//   themeConfig: {
//     nav: [
//       { text: 'Home', link: '/' },
//       { text: 'Guide', link: '/guide/' },
//       { text: 'External', link: 'https://google.com' },
//     ]
//   }
// }

// let blogSideBarPaths = ["/blog/", "/blog/categories/", "/blog/tags/"]

module.exports = {
    // dest: '/home/output/dist',
    title: 'DevOps Docker Container Dokumentation',
    plugins: ['@vuepress/last-updated', '@vuepress/i18n-ui', '@vuepress/back-to-top'],
    themeConfig: {
      nav: [
        { text: 'Home', link: '/' },
        { text: 'Guide', link: '/guide/'},
        { text: 'Debuggen', link: '/debuggen/'},
        /* { text: 'Blog', link: '/blog/' } */
      ],
      //sidebar: {
      //  "/blog/": blogSideBarPaths,
      //  "/blog/tags/": blogSideBarPaths,
      //  "/blog/categories/": blogSideBarPaths,
      //  // fallback
      //  "/": []
      //},
      lastUpdated: {
        label: 'Last Updated', // string
        format: 'YYYY-MM-DDDD hh:mm:ss' // pattern can refer to moment.js
      }
    },
}
