---
title: bla fasel blubb
date: 2019-01-31
datum: "30.01.2019 16:00:00"
description:
    The blog_index front matter will be used to make sure that the blog index isn't listed in the posts. I'm using it when filtering the posts in the posts computed property of the BlogIndex component. Notice how easy it is to include a custom component. It's registered automatically by VuePress and can be included in any markdown file.
---

# second post

The blog_index front matter will be used to make sure that the blog index isn't listed in the posts. I'm using it when filtering the posts in the posts computed property of the BlogIndex component.
Notice how easy it is to include a custom component. It's registered automatically by VuePress and can be included in any markdown file.
