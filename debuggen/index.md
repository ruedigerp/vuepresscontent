---
home: true
sidebar: auto
heroImage: /dev-ops.jpg
heroText: DevOps
tagline: Container debuggen


footer: MIT Licensed | Copyright © 2019-present R. Kuepper
---

## Container debuggen

Startet ein Container nur kurz und man sieht nicht wieso die App im Container nicht startet kann man mit Hilfe der docker-compose.yml den Container mit einer Shell blokieren.

Dazu wird einfach ein command, stdin_open, tty eingefügt:

``` yml
command: >
  sh
stdin_open: true
tty: true
```

Diese drei einfach beim Service eintragen:

``` yml
version: '3.1'

services:

  debugtest:
    image: kaputtesimage
    # deploy:
    #  replicas: 0
    logging:
      driver: json-file
      options:
        max-size: 10m
    ports:
      - 8081:8080
    networks:
      - web
    command: >
      sh
    stdin_open: true
    tty: true

```

Anschließend den Service mit docker-compose up starten und in den Container mit exec und /bin/sh reingehen.

```
$> docker-compose up debugtest
Creating testshell_debugtest_1 ... done
Attaching to testshell_debugtest_1
$> docker exec -it testshell_debugtest_1 /bin/sh
$>
```

Jetzt kann man die App starten und die Fehler suchen. Fehlende Sachen installieren und testen.
Man könnte am Ende dann auch diesen Container als Image committen. Das sollte aber nur in Notfällen gemacht werden.

Alle Änderungen die gemacht wurden sollten entweder im Dockerfile oder der App selbst gefixt werden.
Denn jedes Image muss zu jeder Zeit genau so wieder generiert werden können.
